#ifndef ADRESSEINCORRECTEEX_HPP
#define ADRESSEINCORRECTEEX_HPP
#include <string>

using namespace std;


class AdresseIncorrecteEx : public exception
{
    public:
            AdresseIncorrecteEx();
            AdresseIncorrecteEx(string val);
            virtual ~AdresseIncorrecteEx();

            const char* what() const noexcept
            {
                return cause.c_str();
            }

    private:
        string cause;

};



#endif //ADRESSEINCORRECTEEX_HPP
