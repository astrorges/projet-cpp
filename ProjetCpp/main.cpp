#include <iostream>
#include <stdexcept>

#include "Contact.hpp"
#include "Prive.hpp"
#include "Professionnel.hpp"
#include "ExceptionOnDB.h"
#include "DataBaseHandler.h"
#include "Menu.hpp"



using namespace std;

int Test();
void TestDB();

int main()
{


    DataBaseHandler dbTest1 = DataBaseHandler("./dbContacts.db");
    Menu menu1 = Menu(&dbTest1);


    char choix;
    do{
        menu1.afficherConsole();
        cout << " Quel est votre choix ? " <<endl;
        cin >> choix;
        switch (choix)
        {
        case 'A' :
            menu1.ajouterContact();
            break;

        case 'L' :
            menu1.listerContacts();
            break;

        case 'S' :
            menu1.supprimerContact();
            break;

        case 'R' :
            menu1.recherche();
            break;
        default :
            return 0;
            break;
        }
    }while(choix != 'Q');

    return 0;
}


