#include "Contact.hpp"

Contact::Contact(int identifiant, string nom, string prenom, char sexe)
{
    this->Setidentifiant(identifiant);
    this->Setnom(nom);
    this->Setprenom(prenom);
    this->Setsexe(sexe);
}

Contact::~Contact()
{
    //dtor
}

void Contact::Setidentifiant(int val)
{
    if (val > 0)
    {
       identifiant = val;
    }
    else
    {
        throw std::invalid_argument( "L'identifiant doit etre positif" );
    }
}

void Contact::Setnom(string val)
{
    if (val.length() > 0 && val.length() <= 30)
    {
         for (int i = 0; i < (int) val.length(); i++)
         {
             val[i] = toupper(val[i]);
         }

         nom = val;
    }
    else
    {
         throw std::invalid_argument( "Le nom doit avoir entre 1 et 30 caracteres" );
    }
}

void Contact::Setprenom(string val)
{
    if (val.length() > 0 && val.length() <= 30)
    {
         val[0] = toupper(val[0]);
         for (int i = 1; i < (int) val.length(); i++)
         {
             val[i] = tolower(val[i]);
         }

         prenom = val;
    }
    else
    {
         throw std::invalid_argument( "Le prenom doit avoir entre 1 et 30 caracteres" );
    }
}

void Contact::Setsexe(char val)
{
    val = toupper(val);

    if (val == 'M' || val == 'F')
    {

      sexe = val;

    }
    else
    {
       throw std::invalid_argument( "Le sexe doit avoir un caractere 'M' ou 'F'" );
    }
}
