#ifndef PRIVE_HPP
#define PRIVE_HPP

#include "Contact.hpp"
#include <string>
#include <sstream>
#include <time.h>

class Prive : public Contact
{
    public:
        Prive(int identifiant, string nom, string prenom, char sexe, int num, string rue, string comp, int code, string ville,
              int jour, int mois, int annee);
        virtual ~Prive();

        string Getadresse_postal() { return adresse_postal.affiche(); }
        void Setadresse_postal(int, string, string, int, string);
        string Getdate_de_naissance() { return dateNaissance.affiche(); }
        void Setdate_de_naissance(int, int, int);

        adresse GetStructadresse_postal() {return adresse_postal;}
        date_de_naissance GetDateNaissance() {return dateNaissance;}

        string info() override;
        void getTm(int&, int&, int&);
        string getAge();

    private:
        adresse adresse_postal;
        date_de_naissance dateNaissance;
};

#endif // PRIVE_HPP
