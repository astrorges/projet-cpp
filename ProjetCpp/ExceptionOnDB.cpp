#include <sstream>
#include <iostream>

#include "ExceptionOnDB.h"

using namespace std;

ExceptionOnDB::ExceptionOnDB(string& nvCause,int ncCode ) :
    CodeErreur(ncCode)
{
    ostringstream oss;
    oss << nvCause;
    oss << " avec pour code d'erreur : " << ncCode << endl;

    cause = oss.str();
}

ExceptionOnDB::ExceptionOnDB(const char* nvCause,int ncCode ) :
    CodeErreur(ncCode), cause(nvCause)
{
    ostringstream oss;
    oss << nvCause;
    oss << " avec pour code d'erreur : " << ncCode << endl;

    cause = oss.str();
}


ExceptionOnDB::~ExceptionOnDB()
{
    //dtor
}

const char* ExceptionOnDB::what() const noexcept
{
    return cause.c_str();
}

