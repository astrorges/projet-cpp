#ifndef CONTACT_HPP
#define CONTACT_HPP

#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <sstream>

using namespace std;

typedef struct {
    int numero;
    string rue;
    string complement;
    int code_postal;
    string ville;

    string affiche()
    {
        std::ostringstream oss;
        oss << "\t" << numero << ", " << rue << endl;
        if (complement.empty())
        {
            oss << "\t" << code_postal << " " << ville << endl;
        }
        else {
            oss << "\t" << complement << endl
            << "\t" << code_postal << " " << ville << endl;
        }
        return oss.str();
    }

    //Le format de string pour une requete SQL est different du format d'affichage
    string ToSQLRequest()
    {
        std::ostringstream oss;
        oss << ", '"<< numero << ", "<< rue << "'";
        oss << ", '"<< complement << "'";
        oss << ", "<< code_postal;
        oss << ", '"<< ville << "'";
        return oss.str();
    }
} adresse;


typedef struct {
    int jour;
    int mois;
    int annee;

    string affiche()
    {
        std::ostringstream oss;
        oss << to_string(jour) << "/" << to_string(mois) << "/" << to_string(annee) << endl;
        return oss.str();
    }
    string ToSQLRequest()
    {
        std::ostringstream oss;
        oss << ", '"<< annee << "-"<< mois << "-" << jour << "'";
        return oss.str();
    }

} date_de_naissance;


class Contact
{
    public:
        Contact(int, string, string, char);
        virtual ~Contact();

        int Getidentifiant() { return identifiant; }
        void Setidentifiant(int val);
        string Getnom() { return nom; }
        void Setnom(string val);
        string Getprenom() { return prenom; }
        void Setprenom(string val);
        char Getsexe() { return sexe; }
        void Setsexe(char val);

        virtual string info() = 0;

    private:
        int identifiant;
        string nom;
        string prenom;
        char sexe;
};

#endif // CONTACT_HPP
