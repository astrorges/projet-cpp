#include <iostream>
#include <stdexcept>

#include "ExceptionOnDB.h"
#include "DataBaseHandler.h"
#include <unistd.h>
#include <sstream>
#include <cstring>



DataBaseHandler::DataBaseHandler(string pathDB)
{

    if( access( pathDB.c_str(), F_OK ) == -1 ) {
        // file n'existe pas
        throw ExceptionOnDB("EXCEPTION base de donnée non trouvée ", -1);
    }

    //int rc= sqlite3_open_v2(pathDB.c_str(), &bd, SQLITE_OPEN_READWRITE, "");
    int rc= sqlite3_open(pathDB.c_str(), &bd);
/*
    //TODO Throw erreur !
    int rc= sqlite3_open("./dbContacts.db", &bd);
>>>>>>> creation_sgbd_Clement*/

    if(rc != SQLITE_OK)
    {
        throw ExceptionOnDB("EXCEPTION lors de l'ouverture de la base de donnee", rc);
    }
    else
        cout << "ouverture base avec succes" << endl;

}

DataBaseHandler::~DataBaseHandler()
{
    int rc= sqlite3_close(bd);
    if(rc == SQLITE_OK)
    {
        cout << "fermeture base avec succes" << endl;
    }

}


void DataBaseHandler::effacer(int id)
{
    // Préparez la requête de sélection
    const char *query = "delete from CONTACTS where IdContact=:id";

    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(bd, query, -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        std::cerr << "Erreur lors de la preparation de la requete : " << sqlite3_errmsg(bd) << std::endl;
        //return 1;
    }


    rc = sqlite3_bind_int(stmt, sqlite3_bind_parameter_index(stmt, ":id"), id);
    if (rc != SQLITE_OK) {
        std::cerr << "Erreur lors du binding du parametre : " << sqlite3_errmsg(bd) << std::endl;
        //return 1;
    }

    // Exécutez la requête
    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {

    }

    sqlite3_finalize(stmt);

    if (rc != SQLITE_DONE) {
        std::cerr << "Erreur lors de l'exécution de la requête : ";
    }

    cout << "effacement de : "<< id << endl;
}

void DataBaseHandler::effacer(string nom)
{
    // Préparez la requête de sélection
    const char *query = "delete from CONTACTS where nom=:nom";

    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(bd, query, -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        std::cerr << "Erreur lors de la preparation de la requete : " << sqlite3_errmsg(bd) << std::endl;
        //return 1;
    }


    rc = sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, ":nom"), nom.c_str(),nom.size(),SQLITE_TRANSIENT);
    if (rc != SQLITE_OK) {
        std::cerr << "Erreur lors du binding du parametre : " << sqlite3_errmsg(bd) << std::endl;
        //return 1;
    }

    // Exécutez la requête
    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {

    };

    sqlite3_finalize(stmt);

    if (rc != SQLITE_DONE) {
        std::cerr << "Erreur lors de l'exécution de la requête : ";
    }
    cout << "effacement de : "<< nom << endl;

}


void DataBaseHandler::rechercher(string nom)
{
    // Préparez la requête de sélection
    const char *query = "SELECT * FROM CONTACTS where nom=:nom";

    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(bd, query, -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        std::cerr << "Erreur lors de la preparation de la requete : " << sqlite3_errmsg(bd) << std::endl;
        //return 1;
    }


    rc = sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, ":nom"), nom.c_str(),nom.size(),SQLITE_TRANSIENT);
    if (rc != SQLITE_OK) {
        std::cerr << "Erreur lors du binding du parametre : " << sqlite3_errmsg(bd) << std::endl;
        //return 1;
    }

    // Exécutez la requête
    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        // Récupérez les données des colonnes de la ligne courante


        Contact* nvContact=nullptr;

        creerContact(stmt, &nvContact);

        if(nvContact == nullptr)
            cout << "domage apres creation" << endl;

        cout << nvContact->info() << endl;


        cout << "\n--------------------------------------------\n" << endl;

        delete nvContact;
    }

    sqlite3_finalize(stmt);
    if (rc != SQLITE_DONE) {
        std::cerr << "Erreur lors de l'execution de la requete : ";
    }
    cout << "recherche de : " << nom<< endl;
}


void DataBaseHandler::ajout(Contact *nvContact)
{
    cout << "appel de ajout(Contact *nvContact)" << endl;
    //On construit la requete sql.
    ostringstream oss;
    //L'id est définit automatiquement par le sgbd
    oss << "INSERT INTO Contacts VALUES(NULL";
    oss << ", '"<< nvContact->Getnom() << "'";
    oss << ", '"<< nvContact->Getprenom() << "'";
    oss << ", '"<< nvContact->Getsexe() << "'";

    //il faut maintenant déterminer si le contact est privé ou professionnel.
    Prive* ctpriv=dynamic_cast<Prive *>(nvContact);
    if(ctpriv!=nullptr)
    {
        cout << "Contact privée !!!"<< endl;
        oss << ", ?";//Il n'y a pas d'entreprise
        oss << ctpriv->GetStructadresse_postal().ToSQLRequest();
        oss << ", ?";//pas d'adresse mail
        oss << ctpriv->GetDateNaissance().ToSQLRequest();
    }

    Professionnel* ctprof=dynamic_cast<Professionnel *>( nvContact);
    if(ctprof!=nullptr)
    {
        cout << "Contact professionel !!!"<< endl;
        oss << ", '"<< ctprof->Getnom_entreprise() << "'";
        oss << ctprof->GetStructadresse_entreprise().ToSQLRequest();
        oss << ", '"<< ctprof->Getadresse_mail() << "'";
        oss << ", ?";//pas de date de naissance;
    }

    oss << ");";


    cout << "la requete terminée est :" << endl << oss.str() << endl;

    char *err_msg = 0;
    int rc = sqlite3_exec(bd, oss.str().c_str(), 0, 0, &err_msg);

    if (rc != SQLITE_OK )
    {

        throw ExceptionOnDB("EXCEPTION l'insertion d'un nouveau membre dans la base de donnée", rc);
    }

}


void DataBaseHandler::affichercontact()
{
    affichercontact('i');
    affichercontact('o');
}

void DataBaseHandler::affichercontact(char arg)
{
    ostringstream oss;
    oss << "SELECT * FROM Contacts WHERE ";
    if(arg=='i')
    {
        cout << "affichage des contacts privés " << endl ;
        oss << "mail is null;";
    }
    else if (arg == 'o')
    {
        cout << "affichage des contacts professionnels" << endl;
        oss << "mail is not null;";
    }
    else
    {
        throw std::invalid_argument( "choix d'affichage inconnu ('i' ou 'o' uniquement)" );
    }

    //cout << "la requete est : " << oss.str() << endl;
    sqlite3_stmt *stmt=NULL;
    sqlite3_prepare_v2(bd,
                       oss.str().c_str(),
                       -1,
                       &stmt, NULL);


    int rc;
    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {

        Contact* nvContact=nullptr;

        creerContact(stmt, &nvContact);

        if(nvContact == nullptr)
            cout << "domage apres creation" << endl;

        cout << nvContact->info() << endl;



        cout << "\n--------------------------------------------\n" << endl;
        delete nvContact;
    }

    sqlite3_finalize(stmt);
}

bool DataBaseHandler::isPrive(sqlite3_stmt *stmt)
{
    const unsigned char * dateNaissance=sqlite3_column_text(stmt, 10);

    if(dateNaissance!=NULL)
        return true;
    else
        return false;

}

void DataBaseHandler::creerContact(sqlite3_stmt *stmt, Contact **nvContact)
{
        //Liste des éléments à récupérer pour instancier un individu :
        int id=sqlite3_column_int(stmt, 0);
        string nom( reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1)) );
        string prenom( reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2)) );
        string sexe_string( reinterpret_cast<const char *>(sqlite3_column_text(stmt, 3)) );
        char sexe=sexe_string[0];


        //---- ADRESSE ----
        //On extrait le numéro de rue, pour cela, il faut trouver la première virgule
        string adresseTmp( reinterpret_cast<const char *>(sqlite3_column_text(stmt, 5)) );
        int ptVirgule = adresseTmp.find(',');
        string numAdresse_string=adresseTmp.substr(0, ptVirgule);
        int numAdresse=stoi(numAdresse_string);
        string rueAdresse=adresseTmp.substr(ptVirgule+1, adresseTmp.length() - ptVirgule -1);

        //---Complement d'adresse
        string complementAdresse;//Peut être vide
        const unsigned char * complement=sqlite3_column_text(stmt, 6);
        if(complement == NULL)
                complementAdresse="";
        else
                complementAdresse=reinterpret_cast<const char *>(complement);

        int codePostal =sqlite3_column_int(stmt, 7);
        string ville( reinterpret_cast<const char *>( sqlite3_column_text(stmt, 8)) );
        //---- FIN ADRESSE ----



        if(sqlite3_column_text(stmt, 10) != NULL)
        {
            //----Date de naissance ----
            string dateNaissance( reinterpret_cast<const char *>( sqlite3_column_text(stmt, 10)) );//à séparer en 3, le fromat de la date est predefinie
            int anne=stoi(dateNaissance.substr(0, 4) );
            int mois=stoi(dateNaissance.substr(5, 2) );
            int jour=stoi(dateNaissance.substr(8, 2) );
            //----FIN Date de naissance ----

            (*nvContact) = new Prive(id, nom, prenom, sexe,
                            numAdresse, rueAdresse,
                            complementAdresse,
                            codePostal, ville,
                            jour, mois, anne
                           );

            //cout << individu.info() << endl;
        }
        else if (sqlite3_column_text(stmt, 9) != NULL)
        {
            string nomEntreprise( reinterpret_cast<const char *>(sqlite3_column_text(stmt, 4)) );

            string email( reinterpret_cast<const char *>(sqlite3_column_text(stmt, 9)) );

            (*nvContact) = new Professionnel(id, nom, prenom, sexe,
                                   nomEntreprise,
                                   numAdresse, rueAdresse,
                                   complementAdresse,
                                   codePostal, ville,
                                   email
                                   );
            //cout << individu.info() << endl;

        }
        else
        {
            cout << "DOMAGE!! " << endl;
        }
}
