#ifndef EXCEPTIONONDB_H
#define EXCEPTIONONDB_H

#include <string>

using namespace std;

class ExceptionOnDB
{
    public:
        ExceptionOnDB(string& nvCause,int ncCode);
        ExceptionOnDB(const char* nvCause,int ncCode);
        virtual ~ExceptionOnDB();

        const char* what() const noexcept;

    protected:

    private:
        int CodeErreur;
        string cause;
};

#endif // EXCEPTIONONDB_H
