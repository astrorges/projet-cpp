#ifndef PROFESSIONNEL_HPP
#define PROFESSIONNEL_HPP

#include "Contact.hpp"
#include <string>

#include <sstream>



class Professionnel : public Contact
{
    public:
        Professionnel(int, string, string, char, string, int, string ,string ,int ,string , string);
        virtual ~Professionnel();

        string Getnom_entreprise() { return nom_entreprise; }
        void Setnom_entreprise(string val);
        string Getadresse_entreprise() { return adresse_entreprise.affiche(); }
        void Setadresse_entreprise( int,string ,string ,int ,string );
        string Getadresse_mail() { return adresse_mail; }
        void Setadresse_mail(string val);

        adresse GetStructadresse_entreprise() {return adresse_entreprise;}

        string info() override;
    protected:

    private:
        string nom_entreprise;
        adresse adresse_entreprise;
        string adresse_mail;
};

#endif // PROFESSIONNEL_HPP
