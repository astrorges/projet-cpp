#include "Professionnel.hpp"
#include "Contact.hpp"
#include <string>


Professionnel::Professionnel(int identifiant, string nom,
                             string prenom, char sexe,
                             string nom_entreprise,
                             int numero, string rue,
                            string complement, int code_postal,
                            string ville,string adresse_mail)
    :Contact(identifiant,nom,prenom,sexe)
{
    this->Setnom_entreprise(nom_entreprise);
    this->Setadresse_entreprise(numero,rue,complement,code_postal,ville);
    this->Setadresse_mail(adresse_mail);
}

Professionnel::~Professionnel()
{
    //cout << "Destruction Contact Professionnel" << endl;
}


void Professionnel::Setnom_entreprise(string val)
{
    if (val.length() > 0 && val.length() <= 50)
    {
        for (unsigned int i =0; i < val.length() ; i++ )
        {
            val[i] = toupper(val[i]);
        }
        nom_entreprise = val;
    }
}

void Professionnel::Setadresse_entreprise( int numero, string rue,
                string complement, int code_postal, string ville)
{
    adresse_entreprise.numero = numero;
    adresse_entreprise.rue = rue;
    adresse_entreprise.complement = complement;
    adresse_entreprise.code_postal = code_postal;

    for (unsigned int i = 0; i < ville.length(); i++)
    {
        ville[i] = toupper(ville[i]);
    }

    adresse_entreprise.ville = ville;
    adresse_entreprise.affiche();
}

void Professionnel::Setadresse_mail(string val)
{
    if(val.find('@') < val.length())//On vérifie que l'on trouve un @
    {
        adresse_mail = val;
    }
}


string Professionnel::info()
{

    std::ostringstream oss;
    string appellation;

    if (Getsexe() == 'M')
    {
        appellation = "M.";
    }
    else if (Getsexe() == 'F')
    {
        appellation = "Mme";
    }

    oss << "Professionnel : " << Getidentifiant() << endl
    << "\t" << "Societe " << Getnom_entreprise() << endl
    << "\t" << appellation << Getnom() << " " << Getprenom() << endl
    << Getadresse_entreprise() << endl
    << endl << "\t" << "Adresse mail: " << Getadresse_mail() << endl;

    return oss.str();
}
