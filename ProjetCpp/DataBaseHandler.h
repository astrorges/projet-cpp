#ifndef DATABASEHANDLER_H
#define DATABASEHANDLER_H

#ifdef __linux__
    #include <sqlite3.h>
#endif

#ifdef __WIN32__
    #include "./sqlite-amalgamation-3400000/sqlite3.h"
#endif
#include <string>
#include "Prive.hpp"
#include "Professionnel.hpp"

using namespace std;

class DataBaseHandler
{
    public:
        DataBaseHandler(string="");
        virtual ~DataBaseHandler();

        //pour ajouter un contact Professionnel
        void ajout(Contact *nvContact);
        //pour effacer un contact à partir de son id
        void effacer(int id);
        //pour effacer un contact à partir de son nom et prenom
        void effacer(string nom);
        //pour rechercher un contact à partir de son nom
        void rechercher(string nom);
        //Pour afficher l'ensemble des contacts
        void affichercontact();
        //Pour afficher les contacts privé arg='i' ou professionel arg='o'
        void affichercontact(char arg);


    private:

        //Indique si le stmt pointe vers un contact privé
        bool isPrive(sqlite3_stmt *stmt);
        void creerContact(sqlite3_stmt *stmt, Contact **nvContact);


        sqlite3 *bd;
};

#endif // DATABASEHANDLER_H
