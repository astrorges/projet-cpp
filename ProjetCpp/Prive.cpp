#include "Prive.hpp"

Prive::Prive(int identifiant, string nom, string prenom,
             char sexe, int num, string rue, string comp,
             int code, string ville, int jour, int mois, int annee)
    :Contact(identifiant,nom,prenom,sexe)
{
    this->Setadresse_postal(num, rue, comp, code, ville);
    this->Setdate_de_naissance(jour, mois, annee);
}

Prive::~Prive()
{
    //cout << "Destruction Contact Prive" << endl;
}

void Prive::Setadresse_postal(int num, string rue, string comp, int code, string ville)
{
    this->adresse_postal.numero = num;
    this->adresse_postal.rue = rue;
    this->adresse_postal.complement = comp;
    this->adresse_postal.code_postal = code;

    for (unsigned int i = 0; i < ville.length(); i++)
    {
        ville[i] = toupper(ville[i]);
    }

    this->adresse_postal.ville = ville;
}

void Prive::Setdate_de_naissance(int jour, int mois, int annee)
{
    int md[] = { 31,28,31,30,31,30,31,31,30,31,30,31 };

    if (mois >= 1 && mois <= 12)
    {
        this->dateNaissance.mois = mois;
    }

    else
    {
        throw std::invalid_argument( "Le mois de naissance doit etre entre 1 et 12" );
    }

    if (jour >= 1 && jour <= md[mois-1])
    {
        this->dateNaissance.jour = jour;
    }

    else
    {
        throw std::invalid_argument( "Le jour de naissance invalide" );
    }

    if (annee >= 1900 && annee <= 2022)
    {
        this->dateNaissance.annee = annee;
    }

    else
    {
        throw std::invalid_argument( "L'annee de naissance invalide" );
    }
}

void Prive::getTm(int &jour, int &mois, int &annee)
{
    time_t tt = time(0);
    tm* now = localtime( &tt );

    jour = now->tm_mday;
    mois = now->tm_mon;
    annee = now->tm_year+1900;
}

string Prive::getAge()
{
    std::ostringstream oss;
    int jour_p;
    int mois_p;
    int annee_p;
    int annee_calc;
    //int mois_calc;
    getTm(jour_p, mois_p, annee_p);

    int mois_actuel = mois_p;

    if (this->dateNaissance.jour > jour_p)
    {
        mois_p = mois_p - 1;
    }

    if (this->dateNaissance.mois > mois_p)
    {
        annee_p = annee_p - 1;
    }

    annee_calc = annee_p - this->dateNaissance.annee;


    if ((jour_p == this->dateNaissance.jour) && (mois_actuel == this->dateNaissance.mois))
    {
        oss << to_string(annee_calc) << " ans" << " et Bon Anniversaire" << endl;
    }

    else
    {
        oss << to_string(annee_calc) << " ans" << endl;
    }

    return oss.str();
}

string Prive::info()
{
    string appellation;
    std::ostringstream oss;
    if (Getsexe() == 'M')
    {
        appellation = "M. ";
    }
    else if (Getsexe() == 'F')
    {
        appellation = "Mme";
    }

    oss << "Particulier : " << Getidentifiant()<< endl
    << endl << endl
    << "\t" << appellation << Getnom() << " " << Getprenom() << endl
    <<  Getadresse_postal()
    << "\t" << "Age: " << getAge() << endl;
    return oss.str();
}
