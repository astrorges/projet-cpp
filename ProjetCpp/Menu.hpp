#ifndef MENU_HPP_INCLUDED
#define MENU_HPP_INCLUDED


#include <string>
#include "DataBaseHandler.h"
#include "Contact.hpp"

class Menu
{
    public:
        Menu(DataBaseHandler*);
        virtual ~Menu();

        void afficherConsole();
        void listerContacts();
        void recherche();
        void ajouterContact(Contact *nvContact);
        void supprimerContact();

        DataBaseHandler *dataBase;

};


#endif // MENU_HPP_INCLUDED
