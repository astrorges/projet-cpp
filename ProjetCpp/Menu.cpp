#include "Menu.hpp"
#include <cctype>
#include <sstream>



Menu::Menu(DataBaseHandler *dataBase)
{
    this->dataBase = dataBase;

}

Menu::~Menu()
{

}


void Menu::afficherConsole()
{
    ostringstream oss;

    oss <<"*************************************************" << endl
        << "A - Ajouter Contact" << endl << "L - Lister contact " << endl
        << "R - Rechercher Elements " << endl << "S - Supprimer Contact" << endl
        << "Q - Quitter " << endl
        <<"*************************************************" << endl;

    cout << oss.str() << endl;


}

void Menu::listerContacts()
{
        cout << "Vous avez choisis de lister les contacts, " << endl;
        cout << "Preferez vous afficher les contacts prives(i), professionnels(o) ou tous(t) ? (i/o/t)" << endl;
        char choix;
        cin >> choix;

        choix = tolower(choix);

        switch(choix)
        {
        case 'i':
            this->dataBase->affichercontact('i');
            break;
        case 'o':
            this->dataBase->affichercontact('o');
            break;
        case 't':
            this->dataBase->affichercontact();
            break;
        default:
            cout << "requete non comprise, annulation" << endl;
        }
}

void Menu::recherche()
{
    string input_nom;

    cout << "Vous entrer le nom du contact :" << endl;
    cin >> input_nom;

    dataBase->rechercher(input_nom);
    cout << endl<<endl;
}

void Menu::ajouterContact()
{
    int priv_ou_pro;

    int input_ID;
    string input_nom;
    string input_prenom;
    char input_sexe;
    int input_num_adresse;
    string input_rue_adresse;
    string input_complement_adresse;
    int input_code_postal;
    string input_ville;
    int input_jourdn;
    int input_moisdn;
    int input_anneedn;
    string input_nom_entreprise;
    string input_adresse_mail;

    cout << "Voulez vous ajouter un contact prive (1) ou professionnel (2)?" << endl;
    cin >> priv_ou_pro;


    if (priv_ou_pro == 1)
    {
        cout << "Creation d'un nouveau contact prive" << endl;
        cout << "ID: ";
        cin >> input_ID;

        cout << "Nom: ";
        cin >> input_nom;

        cout << "Prenom: ";
        cin >> input_prenom;

        cout << "Sexe: ";
        cin >> input_sexe;

        cout << "Numero de rue: ";
        cin >> input_num_adresse;

        cout << "Nom de rue: ";
        cin >> input_rue_adresse;

        cout << "Complement d'adresse: ";
        cin >> input_complement_adresse;

        cout << "Code postal: ";
        cin >> input_code_postal;

        cout << "Ville: ";
        cin >> input_ville;

        cout << "Jour de naissance : " << endl;
        cin >> input_jourdn ;

        cout << "Mois de naissance : " << endl;
        cin >> input_moisdn;

        cout << "Annee de naissance : " << endl;
        cin >> input_anneedn;

        Prive nvContact = Prive(input_ID,input_nom,input_prenom,input_sexe,input_num_adresse,input_rue_adresse,input_complement_adresse,input_code_postal,
                                                input_ville,input_jourdn,input_moisdn,input_anneedn);

        this->dataBase->ajout(&nvContact);
    }
    else if(priv_ou_pro == 2)
    {
        cout << "Creation d'un nouveau contact professionnel" << endl;
        cout << "ID: ";
        cin >> input_ID;

        cout << "Nom: ";
        cin >> input_nom;

        cout << "Prenom: ";
        cin >> input_prenom;

        cout << "Sexe: ";
        cin >> input_sexe;

        //adresse
        cout << "Numero de rue: ";
        cin >> input_num_adresse;

        cout << "Nom de rue: ";
        cin >> input_rue_adresse;

        cout << "Complement d'adresse: ";
        cin >> input_complement_adresse;

        cout << "Code postal: ";
        cin >> input_code_postal;

        cout << "Ville: ";
        cin >> input_ville;

        cout<< "Nom de l'entreprise : " << endl;
        cin >> input_nom_entreprise;

        cout << "adresse mail :" << endl;
        cin >> input_adresse_mail;

        Professionnel nvContact = Professionnel(input_ID,input_nom,input_prenom,input_sexe,input_nom_entreprise,input_num_adresse,input_rue_adresse,input_complement_adresse,input_code_postal,
                                                input_ville,input_adresse_mail);

        this->dataBase->ajout(&nvContact);
    }
    else
    {
        cout << "numéro non compris, annulation" << endl;
    }


}

void Menu::supprimerContact()
{
        cout << "Vous avez choisis de supprimer un contact, " << endl;
        cout << "Vous preferez choisir le contact à supprimer par id(i) ou par nom(n) ? (i/n)" << endl;
        char choix;
        cin >> choix;

        choix = tolower(choix);

        if(choix=='i')
        {
            int id;
            cout << "veuillez saisir l'id du contact" << endl;
            cin >> id;
            cout << "id = " << id << endl;
            dataBase->effacer(id);
        }
        else if (choix =='n')
        {
            string nom;
            cout << "veuillez saisir le nom du contact" << endl;
            cin >> nom;
            dataBase->effacer(nom);
        }
        else
        {
            cout << "requete non comprise, annulation" << endl;
        }
}
